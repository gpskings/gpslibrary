from setuptools import setup, find_packages

setup(
    name="gtimes",
    version="0.3",
    description="Time and date modules capable of handling GPS time",
    url="https://gitlab.com/gpskings/gpslibrary/tree/master/gtimes",
    author="Benedikt G. Ofeigsson",
    author_email="bgo@vedur.is",
    license="MIT",
    package_dir={"gtimes": "gtimes"},
    scripts=["timecalc"],
    packages=find_packages(),
    install_requires=["pip>=0.19.1", "pandas", "pytz", "dateutils"],
    zip_safe=False,
)
