# #######################################################
#                                                       #
# dblib.py module installation instructions            #
#                                                       #
# Code made by fjalar@vedur.is                          #
# Iceland Met Office                                    #
# 2013                                                  #
#                                                       #
# #######################################################

Instructions:

  (1) run "python setup.py install" which will install the module in the python domain

All operations must be used with a privileged user (sudo/root)
