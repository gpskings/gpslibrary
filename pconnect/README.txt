# #######################################################
#                                                       #
# pconnect.py and proxytunnel.py modules                #
# installation instructions                             #
#                                                       #
# Instructions made by fjalar@vedur.is                  #
# Iceland Met Office                                    #
# 2013                                                  #
#                                                       #
# #######################################################

Introduction:
pconnect.py is a class module that establishes tunnel connections through 
differnet proxy servers relevant to IMO SIL and GPS operations.

proxytunnel.py is a simple command line script that uses pconnect.py and cparser.py 
to open and close connections.


Instructions:

  (1) run "python setup.py install" which will install the pconnect.py module in the python domain
  (2) run "sh setup.sh" which will place the command line script and it's bash counterpart under /opt/proxytunnel/ and 
      creates a softlink under /usr/bin/ to make it executable.

All operations must be used with a privileged user (sudo/root)
