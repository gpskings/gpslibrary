from setuptools import setup

setup(name='gpsUtils',
      version='0.1',
      description='Utils for the GPS modules',
      url='http://github.com/imo/gps/utils',
      author='Benedikt G. Ofeigsson',
      author_email='bgo@vedur.is',
      license='Icelandic Met Office',
      py_modules=['gpsUtils'],
      )
