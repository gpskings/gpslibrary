from setuptools import setup, find_packages

setup(name='timesfunc',
      version='0.1',
      description='GPS time series manipulations',
      url='http://github.com/imo/geofunc',
      author='Benedikt G. Ofeigsson',
      author_email='bgo@vedur.is',
      license='Icelandic Met Office',
      package_dir={'timesfunc': 'timesfunc'},
      scripts=['bin/compGLOBK'],
      #scripts=['timecalc'],
      packages=find_packages(),
      zip_safe=False)
