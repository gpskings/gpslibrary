#!/bin/bash

echo ""
echo "Copyright (c) 2011-2012 Icelandic Met Office"
echo "CParser 0.1 - Installation script"
echo ""

DIR=/etc/cparser/
FILE=cparser.conf

echo "Creating config file for cparser.py under /etc/cparser/cparser.conf"
echo "Creating directory..."

mkdir $DIR

if [ -d "$DIR"  ]
then
    echo "Copying config file.."
    cp $FILE $DIR/$FILE

    if [ -f "$DIR/$FILE"  ] 
    then
        echo "All seems to have gone well."
	echo ""
	echo "       > DON'T FORGET TO EDIT THE CONFIG FILE"
    else
	echo ""
	echo "ERROR: Config file failed to copy!"
    fi
else
    echo ""	
    echo "ERROR: Directory was not created. User has to have sudo/root privelegs."
fi
