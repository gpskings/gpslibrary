from setuptools import setup, find_packages

setup(name='receivers',
    version='1.0',
    description='Module that provides single interface to multiple GPS receiver types',
    url='https://gitlab.com/gpskings/gpslibrary/tree/master/receivers/dist',
    author='Fjalar Sigurdarson',
    author_email='fjalar@vedur.is',
    license='Icelandic Met Office',
    packages= find_packages(),
    )
